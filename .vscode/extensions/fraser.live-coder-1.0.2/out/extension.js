'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const child_process_1 = require("child_process");
const path = require("path");
const request = require("request-promise-native");
const vscode = require("vscode");
function _failCase() {
    if (vscode.workspace === undefined) {
        vscode.window.showErrorMessage("Must have an open workspace.");
        return true;
    }
    if (vscode.window.activeTextEditor === undefined) {
        vscode.window.showErrorMessage("Must have an active text editor open.");
        return true;
    }
    if (!LiveValuesPanel.isPython(vscode.window.activeTextEditor)) {
        vscode.window.showErrorMessage("Must open a Python file.");
        return true;
    }
    return false;
}
function _newFileCouldHaveLiveValues() {
    return LiveValuesPanel.currentPanel
        && vscode.window.activeTextEditor
        && LiveValuesPanel.isPython(vscode.window.activeTextEditor)
        && LiveValuesPanel.currentPanel.isNewActiveEditor(vscode.window.activeTextEditor);
}
function activate(context) {
    context.subscriptions.push(vscode.commands.registerCommand('liveValues.start', () => {
        if (_failCase()) {
            failedStartBitly();
            return;
        }
        startedBitly();
        var openCount;
        if (context.globalState.get('openCount')) {
            openCount = Number(context.globalState.get('openCount'));
            if (openCount < 5) {
                openCount++;
            }
        }
        else {
            openCount = 1;
        }
        context.globalState.update('openCount', openCount);
        LiveValuesPanel.createOrShow(context.extensionPath, openCount);
        return;
    }));
    context.subscriptions.push(vscode.window.onDidChangeTextEditorVisibleRanges(event => {
        if (LiveValuesPanel.currentPanel && Date.now() - LiveValuesPanel.currentPanel.webviewLastScrolled > 50) {
            scrollPanel(event.textEditor);
        }
    }));
    context.subscriptions.push(vscode.window.onDidChangeActiveTextEditor(() => {
        if (_newFileCouldHaveLiveValues()) {
            LiveValuesPanel.currentPanel.updateWebview(false);
        }
    }));
    context.subscriptions.push(vscode.workspace.onDidSaveTextDocument(() => {
        if (LiveValuesPanel.currentPanel && vscode.window.activeTextEditor) {
            LiveValuesPanel.currentPanel.updateWebview(true);
        }
    }));
}
exports.activate = activate;
function scrollPanel(textEditor) {
    const line = getScrollPosition(textEditor);
    if (typeof line === 'number' && LiveValuesPanel.currentPanel) {
        LiveValuesPanel.currentPanel.scrollWebview(line);
    }
}
class LiveValuesPanel {
    constructor(webViewPanel, extensionPath, openCount) {
        this._disposables = [];
        this._pythonPath = '';
        this._serverPort = 5321;
        this._projectRoot = '';
        this.testsRelativePath = '';
        this.testPattern = '';
        this._selectedTestClassIndex = -1;
        this._selectedtestMethodIndex = -1;
        this._liveValues = {};
        this._callIdToFunction = {};
        this._selectedFunctionCallIds = {};
        this._testOutput = new Array();
        this._currentTestId = "";
        this._testOutputIsClosed = true;
        this.webviewLastScrolled = Date.now();
        this.failedStart = false;
        this._panel = webViewPanel;
        this._extensionPath = extensionPath;
        this._currentActiveTextEditor = vscode.window.activeTextEditor;
        this.openCount = openCount;
        // Listen for when _panel is disposed
        // This happens when the user closes the panel or when the panel is closed programatically
        this._panel.onDidDispose(() => this.dispose(), null, this._disposables);
        if (this._badSettingsError()) {
            return;
        }
        this._startServer().then(() => {
            this._getTestClasses();
            this._handleWebviewMessages();
        });
    }
    static _alreadyHasPanel() {
        return LiveValuesPanel.currentPanel && LiveValuesPanel.currentPanel._panel;
    }
    static _showPanel(column) {
        LiveValuesPanel.currentPanel._panel.reveal(column);
    }
    static _createWebviewPanel(column, extensionPath) {
        return vscode.window.createWebviewPanel(LiveValuesPanel.viewType, 'Live Coder', column, {
            enableScripts: true,
            // Maintain UI even when in background
            retainContextWhenHidden: true,
            // Restrict the webview to only loading content from our extension's `webview_src` directory.
            localResourceRoots: [vscode.Uri.file(path.join(extensionPath, 'webview_src'))]
        });
    }
    static createOrShow(extensionPath, openCount) {
        let panelColumn = vscode.ViewColumn.Beside;
        if (LiveValuesPanel._alreadyHasPanel()) {
            LiveValuesPanel.currentPanel.openCount = openCount;
            LiveValuesPanel._showPanel(panelColumn);
            return;
        }
        const webViewPanel = LiveValuesPanel._createWebviewPanel(panelColumn, extensionPath);
        LiveValuesPanel.currentPanel = new LiveValuesPanel(webViewPanel, extensionPath, openCount);
        if (LiveValuesPanel.currentPanel.failedStart) {
            LiveValuesPanel.currentPanel.dispose();
            return;
        }
    }
    static revive(webViewPanel, extensionPath, openCount) {
        // TODO check this function is ever called.
        LiveValuesPanel.currentPanel = new LiveValuesPanel(webViewPanel, extensionPath, openCount);
    }
    static isPython(activeTextEditor) {
        const fileName = activeTextEditor.document.fileName;
        const potentialPy = fileName.substr(fileName.length - 3);
        return potentialPy === '.py';
    }
    isNewActiveEditor(activeEditor) {
        return activeEditor.document.fileName !== this._currentActiveTextEditor.document.fileName;
    }
    _getTestSettings(pythonPath) {
        this._pythonPath = pythonPath;
        this._projectRoot = vscode.workspace.rootPath;
        const pyTestArgs = vscode.workspace.getConfiguration('python.testing.unittestArgs');
        let i = 0;
        while (pyTestArgs.has(String(i))) {
            let arg = pyTestArgs.get(String(i));
            if (arg && arg[0] !== '-') {
                if (arg.substr(arg.length - 3, 3) === '.py') {
                    this.testPattern = arg;
                }
                else {
                    this.testsRelativePath = arg;
                }
            }
            i++;
        }
    }
    _startingError(message) {
        vscode.window.showErrorMessage(message);
        this._stopStart();
    }
    _stopStart() {
        if (this._serverProcess) {
            this._serverProcess.kill();
        }
        this._panel.dispose();
        this.failedStart = true;
    }
    _usesUnittests() {
        return vscode.workspace.getConfiguration('python.testing').get('unittestEnabled');
    }
    _badSettingsError() {
        const pythonPath = vscode.workspace.getConfiguration('python').get('pythonPath');
        if (!pythonPath) {
            this._startingError('Please select a Python3 interpreter. Do this with the "Python: Select Interpreter" command. Live Coder only works with Python3 unittests.');
            return true;
        }
        if (this._usesUnittests() === false) {
            this._startingError('Please enable unittests in your settings. Do this with the "Python: Configure Tests" command. Live Coder only works with Python3 unittests.');
            return true;
        }
        this._getTestSettings(String(pythonPath));
        if (this._projectRoot === '' || this.testsRelativePath === '' || this.testPattern === '') {
            this._startingError('Please update your settings for "python.testing.unittestArgs". Do this with the "Python: Configure Tests" command. They must include a test folder and test pattern matcher.');
            return true;
        }
        return false;
    }
    _installServer() {
        return __awaiter(this, void 0, void 0, function* () {
            var installPromise = new Promise((resolve, reject) => {
                exec(`${this._pythonPath} -m pip install --upgrade live-coder`, (error, stdout, stderr) => {
                    if (error) {
                        reject(stderr);
                    }
                    else {
                        resolve('');
                    }
                });
            });
            var timeoutPromise = new Promise(function (resolve, reject) {
                setTimeout(resolve, 7000, "Took too long to install/update the server with pip. To run yourself use `pip install -U live-coder`");
            });
            return Promise.race([installPromise, timeoutPromise]).then(function (error) {
                if (error) {
                    vscode.window.showErrorMessage(String(error));
                }
            });
        });
    }
    _getLines(text) {
        const lines = text.split('\n');
        return '<span>' + lines.join('</span><span>') + '</span>';
    }
    _startServer() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this._serverProcess) {
                return;
            }
            yield this._installServer();
            this._serverProcess = child_process_1.spawn(`${this._pythonPath} -m live_coder ${this._serverPort}`, [], { shell: true });
            var stdOut = "";
            this._serverProcess.stdout.setEncoding('utf8');
            this._serverProcess.stdout.on('data', function (data) {
                stdOut += data.toString();
            });
            var stdErr = "";
            this._serverProcess.stderr.setEncoding('utf8');
            this._serverProcess.stderr.on('data', function (data) {
                stdErr += data.toString();
            });
            this._serverProcess.on('exit', code => {
                const outLines = this._getLines(stdOut);
                const errLines = this._getLines(stdErr);
                this._errorHTML(`<h2>Server stopped unexpectedly. Check nothing is running on port ${this._serverPort}<h2>
				<p>${outLines}</p>
				<p>${errLines}</p>
			`);
            });
            this._serverProcess.on('error', (error) => __awaiter(this, void 0, void 0, function* () {
                serverFailedBitly();
                const outLines = this._getLines(stdOut);
                const errLines = this._getLines(stdErr);
                this._errorHTML(`<h2>Server failed. Check nothing is running on port ${this._serverPort}<h2>
				<p>${outLines}</p>
				<p>${errLines}</p>
			`);
            }));
            yield this._sleep(2000); // Give the server time to start.
        });
    }
    _sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        });
    }
    _getTestClasses() {
        let testClassesPromise = newProjectSession(this._serverPort, this._projectRoot, this.testsRelativePath, this.testPattern);
        testClassesPromise.then((response) => {
            if (response.type === 'error') {
                this._startingError(response.message);
                return;
            }
            this._testClasses = response.testClasses;
            this.updateWebview(true);
        });
        testClassesPromise.catch(error => {
            this._panel.webview.html = this._errorHTML('<b>Setting up the server.</b> Will take a few seconds.');
        });
    }
    _scrollToLine(line) {
        this.webviewLastScrolled = Date.now();
        const range = new vscode.Range(line, 0, line + 1, 0);
        this._currentActiveTextEditor.revealRange(range, vscode.TextEditorRevealType.AtTop);
    }
    _selectNoTestClass() {
        this._selectedTestClassIndex = -1;
        this._selectedtestMethodIndex = -1;
        this._currentTestId = "";
        this._panel.webview.html = this._errorHTML('<b>No Test Class or Test Method Selected</b> Use the dropdown above to see your code run!');
    }
    _runTestMethod(classIndex, methodIndex, method) {
        this._currentTestId = method;
        let response = this._getLiveValues();
        response.then((liveValuesAndTestOutput) => {
            if (liveValuesAndTestOutput === undefined) {
                this._panel.webview.html = this._errorHTML('<b>Error</b> Got no response from the server, is it running?');
            }
            else {
                this._selectedTestClassIndex = classIndex;
                this._selectedtestMethodIndex = methodIndex;
                this._panel.webview.html = this._liveValuesHTML(liveValuesAndTestOutput[0], liveValuesAndTestOutput[1]);
            }
        });
    }
    _handleWebviewMessages() {
        this._panel.webview.onDidReceiveMessage(message => {
            switch (message.command) {
                case 'revealLine':
                    this._scrollToLine(message.line);
                    return;
                case 'clearLiveValues':
                    this._selectNoTestClass();
                    return;
                case 'runTestMethod':
                    this._runTestMethod(message.classIndex, message.methodIndex, message.method);
                    return;
                case 'toggleTestOutput':
                    this._testOutputIsClosed = this._testOutputIsClosed === false;
                case 'openFunctionCall':
                    this._openFunctionCall(message.callId, message.name);
                case 'updateFunctionCallSelection':
                    this._updateFunctionCallSelection(message.callId, message.name);
            }
        }, null, this._disposables);
    }
    scrollWebview(yPosition) {
        this._panel.webview.postMessage({ command: 'editorScroll', scroll: yPosition });
    }
    dispose() {
        this._testClasses = null;
        LiveValuesPanel.currentPanel = undefined;
        this._panel.dispose();
        while (this._disposables.length) {
            const x = this._disposables.pop();
            if (x) {
                x.dispose();
            }
        }
    }
    _updateFileAttributes() {
        if (vscode.window.activeTextEditor) {
            this._currentActiveTextEditor = vscode.window.activeTextEditor;
        }
        this._panel.title = `Live Coder: ` + this._currentFileNameShort();
    }
    _updatePanelDisplay(reloadValues) {
        let response;
        if (reloadValues) {
            response = this._getLiveValues();
        }
        else {
            response = new Promise((resolve, reject) => {
                resolve(this._getLiveValuesForCurrentEditor());
            });
        }
        response.then((liveValuesAndTestOutput) => {
            if (liveValuesAndTestOutput === undefined) {
                this._panel.webview.html = this._errorHTML('<b>Error</b> Got no response from the server, is it running?');
            }
            else {
                this._panel.webview.html = this._liveValuesHTML(liveValuesAndTestOutput[0], liveValuesAndTestOutput[1]);
            }
            scrollPanel(this._currentActiveTextEditor);
        });
    }
    updateWebview(reloadValues) {
        updateWebViewBitly();
        this._updateFileAttributes();
        this._updatePanelDisplay(reloadValues);
    }
    _liveValuesHTML(liveValues, testOutput) {
        const testClassOptions = this._getTestClassOptions();
        const testMethodOptions = this._getTestMethodOptions();
        return this._newHTMLdoc(`
			<div id="header">
				<select class="picker" id="testClassPicker">
					<option value="">No Test Class</option>
					<option value="─────────" disabled="">─────────</option>
					${testClassOptions}
				</select>
				<select class="picker" id="testMethodPicker">
					${testMethodOptions}
				</select>
				<a id="issueLink" href="https://gitlab.com/Fraser-Greenlee/live-coder-vscode-extension/issues/new">report an issue</a>
			</div>
			<div id="scrollableLiveValues">
				${liveValues}
				<div id="tooltipBackground"></div>
			</div>
			${testOutput}`);
    }
    _errorHTML(message) {
        return this._newHTMLdoc(`
			<div id="scrollableLiveValues">
				<div class="centre"><span>
					${message}
				</span></div>
			</div>
		`);
    }
    _newHTMLdoc(body) {
        // Local path to main script run in the webview
        const scriptPathOnDisk = vscode.Uri.file(path.join(this._extensionPath, 'webview_src', 'main.js'));
        // And the uri we use to load this script in the webview
        const scriptUri = scriptPathOnDisk.with({ scheme: 'vscode-resource' });
        // do above for main.css
        const cssPathOnDisk = vscode.Uri.file(path.join(this._extensionPath, 'webview_src', 'main.css'));
        const cssUri = cssPathOnDisk.with({ scheme: 'vscode-resource' });
        // Use a nonce to whitelist which scripts can be run
        const nonce = getNonce();
        return `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">

                <!--
                Use a content security policy to only allow loading scripts that have a specific nonce.

				<meta http-equiv="Content-Security-Policy" content="default-src 'none'; script-src 'nonce-${nonce}'; style-src 'nonce-${nonce}';">
				<meta http-equiv="Content-Security-Policy" content="default-src 'none'; script-src vscode-resource:; style-src 'unsafe-inline' vscode-resource:;">
				-->

				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link rel="stylesheet" type="text/css" nonce="${nonce}" href="${cssUri}">

                <title>Live Coder</title>
            </head>
			<body style="margin-top: 5px;">
				${body}
				<div class="highlight" id="tooltip"></div>
                <script nonce="${nonce}" src="${scriptUri}"></script>
            </body>
            </html>`;
    }
    _testClassNameFromId(testClassId) {
        const idParts = testClassId.split('.');
        return idParts[idParts.length - 1];
    }
    _testFileNameFromId(testClassId) {
        const idParts = testClassId.split('.');
        return idParts.slice(1, idParts.length - 1).join('/') + '.py';
    }
    _testClassNames() {
        let names = new Array(this._testClasses.length);
        for (let i = 0; i < this._testClasses.length; i++) {
            const testClass = this._testClasses[i];
            names[i] = this._testClassNameFromId(testClass.id);
        }
        return this._handleDuplicateClassNames(names);
    }
    _appendFilePathToDuplicateTestClassNames(names, duplicateIndices) {
        for (let i = 0; i < duplicateIndices.length; i++) {
            const classIndex = duplicateIndices[i];
            const testClass = this._testClasses[i];
            names[classIndex] = `${this._testClassNameFromId(testClass.id)} ---- ${this._testFileNameFromId(testClass.id)}`;
        }
        return names;
    }
    _handleDuplicateClassNames(names) {
        let duplicateNameToIndices = getDuplicates(names);
        if (duplicateNameToIndices.size > 0) {
            const duplicateNames = Array.from(duplicateNameToIndices.keys());
            for (let i = 0; i < duplicateNames.length; i++) {
                const duplicateName = duplicateNames[i];
                const duplicateIndices = duplicateNameToIndices.get(duplicateName);
                names = this._appendFilePathToDuplicateTestClassNames(names, duplicateIndices);
            }
        }
        return names;
    }
    _selectedProperty(selectedIndex, i) {
        if (i === selectedIndex) {
            return 'selected';
        }
        else {
            return '';
        }
    }
    _testClassOption(ClassNames, ClassIndex) {
        const name = ClassNames[ClassIndex];
        const testClass = this._testClasses[ClassIndex];
        const selected = this._selectedProperty(this._selectedTestClassIndex, ClassIndex);
        return `<option value="${testClass.id}" data-method_names="${testClass.method_names}" data-method_ids="${testClass.method_ids}" ${selected}>${name}</option>`;
    }
    _getTestClassOptions() {
        let classNames = this._testClassNames();
        let options = new Array(this._testClasses.length);
        for (let i = 0; i < classNames.length; i++) {
            options[i] = this._testClassOption(classNames, i);
        }
        return options.join('');
    }
    _getTestMethodOptions() {
        if (this._selectedTestClassIndex === -1) {
            return '<option value="">No Test Method</option>';
        }
        else {
            return this._testMethodOptionsForClass(this._testClasses[this._selectedTestClassIndex]);
        }
    }
    _testMethodOptionsForClass(testClass) {
        let options = new Array(testClass.method_names.length);
        for (let i = 0; i < testClass.method_names.length; i++) {
            const selected = this._selectedProperty(this._selectedtestMethodIndex, i);
            options[i] = `<option value="${testClass.method_ids[i]}" ${selected}>${testClass.method_names[i]}</option>`;
        }
        return options.join('');
    }
    _callIdsForFile(filePath) {
        const selectedCallIds = this._selectedFunctionCallIds[filePath];
        if (selectedCallIds === undefined) {
            return {};
        }
        return selectedCallIds;
    }
    _validCallId(callsToValues, selectedCallId) {
        if (selectedCallId) {
            return selectedCallId in callsToValues;
        }
        return false;
    }
    _firstFunctionCall(callsToValues) {
        const calls = Object.keys(callsToValues);
        calls.sort();
        return calls[0];
    }
    _selectedCallIdForFunction(callsToValues, selected) {
        if (!this._validCallId(callsToValues, selected)) {
            return this._firstFunctionCall(callsToValues);
        }
        return selected;
    }
    _selectedCallIdsForFile(filePath) {
        const fileFunctions = this._liveValues[filePath];
        let selectedCallIds = this._callIdsForFile(filePath);
        Object.keys(fileFunctions).forEach(functionName => {
            selectedCallIds[functionName] = this._selectedCallIdForFunction(fileFunctions[functionName]['calls'], selectedCallIds[functionName]);
        });
        return selectedCallIds;
    }
    _getSelectedFunctionCallIds() {
        Object.keys(this._liveValues).forEach(filePath => {
            this._selectedFunctionCallIds[filePath] = this._selectedCallIdsForFile(filePath);
        });
        return this._selectedFunctionCallIds;
    }
    _liveValuesErrorMessage(title, body) {
        if (this.openCount >= 5) {
            return new Array(`<div class="centre">
					<div class="widthLimiter">
						<span><b>${title}</b> ${body}</span>
						<div id="postHolder">
							<div class="post" style="margin: 0px;">Please complete this <a href="https://forms.gle/7W5qATvzuqtpnTKZ6">short survey</a> so I can improve Live Coder.</div>
						</div>
					</div>
				</div>`, '');
        }
        return new Array(`<div class="centre">
				<div class="widthLimiter">
					<span><b>${title}</b> ${body}</span>
					<div id="postHolder">
						<div class="post">
							<h2>What's new in v1.0!</h2>
							<ul>
								<li><b>No more server</b>: Just run the extension and it works!</li>
								<li><b>goto links</b>: Click <span class="function_call_link sample">from function_name</span> links goto where functions were called from.</li>
								<li><b>Click to expand</b>: Click on values to see expanded versions.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>`, '');
    }
    _noLiveValuesResponse() {
        if (this._currentTestId === "") {
            return this._liveValuesErrorMessage('No active test.', 'Select a test class and method from the dropdown above.');
        }
        if (this._currentActiveTextEditor === undefined) {
            return this._liveValuesErrorMessage('No active editor.', 'Open a Python file to see it run.');
        }
        return null;
    }
    _liveValuesResponseError(response) {
        if (response.errorType === 'ExtensionError') {
            this.dispose();
        }
        else if (response.errorType === 'ImportError') {
            response.message = response.message;
        }
        vscode.window.showErrorMessage(response.message);
    }
    _assignLiveValuesAttributesFromResponse(response) {
        this._liveValues = response.live_values;
        this._callIdToFunction = response.call_id_to_function;
        this._selectedFunctionCallIds = this._getSelectedFunctionCallIds();
        this._testOutput = response.test_output.split('\n');
        const testClasses = response.test_classes;
        this._testClasses = testClasses;
    }
    _getLiveValues() {
        return __awaiter(this, void 0, void 0, function* () {
            const failResponse = this._noLiveValuesResponse();
            if (failResponse) {
                return failResponse;
            }
            const response = yield requestLiveValues(this._pythonPath, this._projectRoot, this._serverPort, this._currentTestId, this.testsRelativePath, this.testPattern);
            if (response.type === 'error') {
                this._liveValuesResponseError(response);
            }
            this._assignLiveValuesAttributesFromResponse(response);
            return this._getLiveValuesForCurrentEditor();
        });
    }
    _testStatus() {
        if (this._testOutput[0] === "F") {
            return 'Failed';
        }
        else if (this._testOutput[0] === "E") {
            return 'Error';
        }
        return 'Passed';
    }
    _testResizeClass() {
        if (this._testOutputIsClosed) {
            return 'closed';
        }
        return 'open';
    }
    _testOutputArrow() {
        if (this._testOutputIsClosed) {
            return '&#8594';
        }
        return '&#8600';
    }
    _getTestOutput() {
        const testOutputHTML = this._linesAsHTML(this._testOutput);
        const testStatus = this._testStatus();
        const resizeClass = this._testResizeClass();
        const arrowString = this._testOutputArrow();
        return `<div id="testPanel"><div id="resize" class="${resizeClass}">
			<div id="testPanelResizer"></div>
			<div id="testHeader">
				<span id="testOuptutArrow">${arrowString}</span><b>Test Output:</b><span id="testStatus" class="test${testStatus}">${testStatus}</span>
			</div>
			<div id="testBody">
				${testOutputHTML}
			</div>
		</div></div>`;
    }
    _noValuesForFile() {
        return new Array(`<div class="centre">
				<span>
					<b>File not ran by the selected test.</b>
					<span class="function_call_link clearLink" data-reference-id="start" data-reference-name="${this._callIdToFunction.start[1]}">open test start</span>
				</span>
			</div>`, this._getTestOutput());
    }
    _hideFunctionCall(selectedCallId, callId) {
        if (selectedCallId !== callId) {
            return 'hide';
        }
        return '';
    }
    _htmlForFunctionCall(functionName, callId, html, selectedCallId) {
        const hide = this._hideFunctionCall(selectedCallId, callId);
        return `<div class="functionCall ${hide} functionName_${functionName}" id="FunctionCall_${callId}" data-reference-id="${callId}" data-reference-name="${functionName}">${html}</div>`;
    }
    _disabledCallSelector(numberOfCalls) {
        if (numberOfCalls > 1) {
            return '';
        }
        return 'disabled';
    }
    _functionButtons(functionName, calls) {
        const disabled = this._disabledCallSelector(Object.keys(calls).length);
        let buttons = `<button class="functionCallButton previous ${disabled}" data-functionName="${functionName}">&lt;</button>`;
        buttons += `<button class="functionCallButton next ${disabled}" data-functionName="${functionName}">&gt;</button>`;
        return buttons;
    }
    _functionCallIds(calls) {
        let callIds = [];
        Object.keys(calls).forEach(callId => {
            callIds.push(`functionCall${callId}`);
        });
        return callIds.join(' ');
    }
    _htmlForAFunction(functionInfo, selectedCallId, functionName) {
        let functionCallsHTML = new Array();
        Object.keys(functionInfo.calls).forEach(callId => {
            functionCallsHTML.push(this._htmlForFunctionCall(functionName, callId, functionInfo.calls[callId], selectedCallId));
        });
        const callIds = this._functionCallIds(functionInfo.calls);
        const buttons = this._functionButtons(functionName, functionInfo.calls);
        return `<div class="function" id="${callIds}" style="top: ${(functionInfo.starting_line_number - 1) * 18}px">${buttons}${functionCallsHTML.join('')}</div>`;
    }
    _htmlForFunctions(functionsToCalls, selectedFunctionCallIds) {
        let HTMLFunctions = new Array();
        Object.keys(functionsToCalls).forEach(functionName => {
            HTMLFunctions.push(this._htmlForAFunction(functionsToCalls[functionName], selectedFunctionCallIds[functionName], functionName));
        });
        return HTMLFunctions.join('');
    }
    _getLiveValuesForCurrentEditor() {
        const FunctionsToCalls = this._liveValues[this._currentFileName()];
        const selectedFunctionCallIds = this._selectedFunctionCallIds[this._currentFileName()];
        if (FunctionsToCalls === undefined) {
            return this._noValuesForFile();
        }
        let functionsHTML = this._htmlForFunctions(FunctionsToCalls, selectedFunctionCallIds);
        const testOutputHTML = this._getTestOutput();
        let lineCount = this._currentActiveTextEditor.document.lineCount + 100;
        return new Array(`<div class="padding" style="padding-bottom: ${lineCount * 18}px" onclick="console.log('clicked padding')"></div>${functionsHTML}`, testOutputHTML);
    }
    _linesAsHTML(linesContent) {
        var HTMLlines = new Array(linesContent.length);
        for (let i = 0; i < linesContent.length; i++) {
            HTMLlines[i] = `<div style="height:18px;" class="view-line"><span>${linesContent[i]}</span></div>`;
        }
        return HTMLlines.join('');
    }
    _currentFileNameShort() {
        const fullPath = this._currentActiveTextEditor.document.fileName;
        return fullPath.split('/').pop();
    }
    _currentFileName() {
        const fullPath = this._currentActiveTextEditor.document.fileName;
        const projectRootTerms = this._projectRoot.split('/');
        const pathTerms = fullPath.split('/');
        const localPathTerms = pathTerms.slice(projectRootTerms.length);
        return localPathTerms.join('/');
    }
    _openFunctionCall(callId, name) {
        const path = `${this._projectRoot}/${this._callIdToFunction[callId][0]}`;
        vscode.workspace.openTextDocument(path).then(doc => {
            vscode.window.showTextDocument(doc, vscode.ViewColumn.One);
        }, err => {
            console.log(err);
        });
    }
    _updateFunctionCallSelection(callId, name) {
        this._selectedFunctionCallIds[this._currentFileName()][name] = callId;
    }
}
LiveValuesPanel.viewType = 'liveValues';
function getNonce() {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 32; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}
function getScrollPosition(editor) {
    if (!editor.visibleRanges.length) {
        return undefined;
    }
    const firstVisiblePosition = editor.visibleRanges[0].start;
    const lineNumber = firstVisiblePosition.line;
    const line = editor.document.lineAt(lineNumber);
    const progress = firstVisiblePosition.character / (line.text.length + 2);
    return (lineNumber + progress) * 18;
}
function startedBitly() {
    return __awaiter(this, void 0, void 0, function* () {
        var options = {
            method: 'GET',
            uri: `http://bit.ly/2kgZWjN`,
        };
        yield request.get(options);
    });
}
function failedStartBitly() {
    return __awaiter(this, void 0, void 0, function* () {
        var options = {
            method: 'GET',
            uri: `http://bit.ly/2ltwebx`,
        };
        yield request.get(options);
    });
}
function serverFailedBitly() {
    return __awaiter(this, void 0, void 0, function* () {
        var options = {
            method: 'GET',
            uri: `http://bit.ly/2lYrtqD`,
        };
        yield request.get(options);
    });
}
function updateWebViewBitly() {
    return __awaiter(this, void 0, void 0, function* () {
        var options = {
            method: 'GET',
            uri: `http://bit.ly/2jXaDI1`,
        };
        yield request.get(options);
    });
}
function newProjectSession(serverPort, projectRoot, testsRelativePath, testPattern) {
    return __awaiter(this, void 0, void 0, function* () {
        var options = {
            method: 'POST',
            uri: `http://0.0.0.0:${serverPort}/new_project`,
            json: true,
            body: {
                root_path: projectRoot,
                tests_relative_path: testsRelativePath,
                test_pattern: testPattern
            }
        };
        const response = yield request.post(options);
        return response;
    });
}
function requestLiveValues(pythonPath, projectRoot, serverPort, testId, testsRelativePath, testPattern) {
    return __awaiter(this, void 0, void 0, function* () {
        var options = {
            method: 'POST',
            uri: `http://0.0.0.0:${serverPort}/live_values`,
            json: true,
            body: {
                python_path: pythonPath,
                root_path: projectRoot,
                tests_relative_path: testsRelativePath,
                test_pattern: testPattern,
                test_id: testId
            }
        };
        const response = yield request.post(options);
        return response;
    });
}
function getDuplicates(array) {
    var duplicates = new Map();
    for (var i = 0; i < array.length; i++) {
        if (duplicates.has(array[i])) {
            duplicates.get(array[i]).push(i);
        }
        else if (array.lastIndexOf(array[i]) !== i) {
            duplicates.set(array[i], [i]);
        }
    }
    return duplicates;
}
//# sourceMappingURL=extension.js.map